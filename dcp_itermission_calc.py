#!/usr/bin/env python3

"""
DCP Intermission Calculator

Authors:
    Joe Mullally (jwmullally@gmail.com)
    Vivi Sereti (vivithegoddess@gmail.com)

LICENSE: MIT
"""

import re
import sys


def seconds_to_time(seconds):
    hours_result, hrs_seconds = divmod(seconds, 3600)
    minutes_result, seconds_result = divmod(hrs_seconds, 60)
    time_str = '%02d:%02d:%02d' % (hours_result, minutes_result, seconds_result)
    return time_str
    
def frames_to_time(num_frames):
    seconds = round(num_frames / 24)
    time = seconds_to_time(seconds)
    return time

def reels_to_time (reels):
    result_times = []
    total_frames = 0
    for frames in reels:
        total_frames = total_frames + frames
        time = frames_to_time(total_frames)
        result_times.append (time)
    return result_times

def xml_get_reel_frames(xmlstr):
    """
    Return a list of MainPicture Reel durations in frames from a DCP xml
    Example:
        get_reel_frames(xmlstr)
        > [29809, 30000, 30000, 30000, 3911]
    """
    reel_frames = []
    for mainpic in re.findall('<MainPicture>(.*?)</MainPicture>', xmlstr):
        fps_str = re.findall('<FrameRate>(.*?)</FrameRate>', mainpic)[0]
        if fps_str != '24 1':
            raise ValueError('Unhandled FrameRate: ' + fps_str)
        duration = int(re.findall('<Duration>(.*?)</Duration>', mainpic)[0])
        reel_frames.append(duration)
    return reel_frames

if len(sys.argv) == 1:
    print('1: Enter list manually')
    print('2: Read from XML')
    mode = input('enter mode: ')
else:
    print('Reading XML file from command line')
    mode = '2'

if mode == '1':
    print('Enter Reel Durations, seperated by space')
    print('Example:')
    print('23950 29584 23986 21958')
    frames_input = input('> ')
    reel_frames = [int(x) for x in frames_input.split()]   
elif mode == '2':
    if len(sys.argv) > 1:
        xml_file = sys.argv[1]
    else:
        xml_file = input('Enter XML file path: ')
    print('Reading XML CompositionPlayList from {}'.format(xml_file))
    xmlstr = open(xml_file).read()
    xmlstr = xmlstr.replace('\n', '')
    if '<CompositionPlaylist' not in xmlstr or '<ContentKind>feature</ContentKind>' not in xmlstr:
        raise ValueError('File does not look like a DCP Feature playlist XML')
    title = re.findall('<ContentTitleText>(.*?)</ContentTitleText>', xmlstr)[0]
    print('Title:', title)
    reel_frames = xml_get_reel_frames(xmlstr)
else:
    print('Unknown mode, please enter 1 or 2')
    exit(1)

#reel_frames = [23950, 29584, 23986, 21958]
print ('Input reels Durations (in frames):', reel_frames)
result = reels_to_time(reel_frames)
print ('Reel end times (HH:MM:SS): ', result)
input ('Hit Return to exit')